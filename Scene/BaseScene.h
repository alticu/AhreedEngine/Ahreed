#ifndef __BASESCENE_H
#define __BASESCENE_H

#include <cstdint>

namespace Ahreed {
namespace Graphics {
class BaseRenderer;
};

namespace Scene {

class BaseScene {
   public:
    virtual void render(Graphics::BaseRenderer* renderer) = 0;
    virtual bool onKeyPressed(unsigned int code, uint8_t state) = 0;

    bool active = false;
};

};  // namespace Scene
};  // namespace Ahreed

#endif
