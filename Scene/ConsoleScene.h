#ifndef __CONSOLESCENE_H
#define __CONSOLESCENE_H

#include <string>
#include <vector>
#include "BaseScene.h"

#ifndef COMMAND_BUFFER_SIZE
#define COMMAND_BUFFER_SIZE 256
#endif

namespace Ahreed {
namespace Scene {

class ConsoleScene : public BaseScene {
   public:
    ConsoleScene();
    void render(Graphics::BaseRenderer* renderer);

    bool onKeyPressed(unsigned int code, uint8_t state);
    static void onLog(int type, const char* msg);

   private:
    static std::vector<std::pair<int, std::string>> _scrollback;
    static std::vector<std::string> _history;

    char _commandBuffer[COMMAND_BUFFER_SIZE];
};

};  // namespace Scene
};  // namespace Ahreed

#endif
