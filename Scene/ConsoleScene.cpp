#include "ConsoleScene.h"

#include "../Graphics/Renderer/BaseRenderer.h"
#include "imgui.h"

using namespace Ahreed::Scene;

std::vector<std::pair<int, std::string>> ConsoleScene::_scrollback = {};

ConsoleScene::ConsoleScene() { _commandBuffer[0] = '\0'; }

void ConsoleScene::render(Graphics::BaseRenderer* renderer) {
    bool show = true;
    if (!ImGui::Begin("Debug Console", &active)) {
        ImGui::End();
        return;
    }
    ImGui::BeginChild("scrollback", ImVec2(0, -30));
    for (auto a : ConsoleScene::_scrollback) {
        ImVec4 color(0.2f, 0.2f, 0.2f, 1.f);
        ImGui::Text("%s", a.second.c_str());
    }
    ImGui::SetScrollHere();
    ImGui::EndChild();
    ImGui::InputText("", _commandBuffer, COMMAND_BUFFER_SIZE);
    ImGui::End();
}

void ConsoleScene::onLog(int type, const char* msg) {
    std::string s_msg(msg);
    ConsoleScene::_scrollback.emplace_back(type, s_msg);
}

bool ConsoleScene::onKeyPressed(unsigned int code, uint8_t state) {
    ConsoleScene::_scrollback.emplace_back(0, "Key pressed!");
    if (code == 96 && state == 1) {
        active = !active;
        return active;
    }
    if (!active) return false;
    return true;
}
