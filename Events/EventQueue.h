#ifndef __EVENTQUEUE_H
#define __EVENTQUEUE_H

namespace Ahreed {
namespace Events {

class Observer {
    friend class Subject;

   public:
    virtual ~Observer();
    virtual void onNotify() = 0;

   private:
    Observer* _prev;
    Observer* _next;
};

class Subject {
   public:
    Subject();

    void addObserver(Observer* observer);
    void removeObserver(Observer* observer);

   protected:
    void notify();

   private:
    Observer* _head;
};

};  // namespace Events
};  // namespace Ahreed

#endif
