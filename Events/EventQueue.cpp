#include "EventQueue.h"

using namespace Ahreed::Events;

Subject::Subject() : _head(nullptr) {}

void Subject::addObserver(Observer* observer) {
    if (_head == nullptr) {
        _head = observer;
    } else {
        observer->_next = _head;
        _head->_prev = observer;
        _head = observer;
    }
}

void Subject::removeObserver(Observer* observer) {
    if (observer == _head) {
        observer->_next->_prev = nullptr;
        _head = observer->_next;
    } else {
        observer->_next->_prev = observer->_prev;
        observer->_prev->_next = observer->_next;
    }
}

void Subject::notify() {
    Observer* o = _head;
    while (o != nullptr) {
        o->onNotify();
        o = o->_next;
    }
}
