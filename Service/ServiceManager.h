#ifndef __SERVICEMANAGER_H
#define __SERVICEMANAGER_H

#include <cstdint>

namespace Ahreed {

namespace Graphics {
class BaseWindow;
};

namespace Service {

class ServiceManager {
   public:
    static Graphics::BaseWindow* getWindow() { return _window; }

    static void provide(Graphics::BaseWindow* window) { _window = window; }

   private:
    static Ahreed::Graphics::BaseWindow* _window;
};

class CallbackManager {
   public:
    static void keyPressedCallback(unsigned int code, uint8_t state);
};

};  // namespace Service
};  // namespace Ahreed

#endif
