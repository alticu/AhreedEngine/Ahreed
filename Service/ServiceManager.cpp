#include "ServiceManager.h"

#include "../Graphics/Window/BaseWindow.h"

using namespace Ahreed::Service;

Ahreed::Graphics::BaseWindow *ServiceManager::_window;

void CallbackManager::keyPressedCallback(unsigned int code, uint8_t state) {
    ServiceManager::getWindow()->keyPressedCallback(code, state);
}
