#ifndef __SCRIPTHELPER_H
#define __SCRIPTHELPER_H

#include <angelscript.h>
#include <string>

namespace Ahreed {
namespace Scripting {

class ScriptHelper {
   public:
    ScriptHelper();
    ~ScriptHelper();

    bool loadFile(const std::string &filename);

    bool runCode(const std::string &code);

   protected:
    static void MessageCallback(const asSMessageInfo *msg, void *param);
    static void PrintString(const std::string &msg);

   private:
    asIScriptEngine *_engine;
    asIScriptContext *_ctx;
};

};  // namespace Scripting
};  // namespace Ahreed

#endif
