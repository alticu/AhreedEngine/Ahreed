#include "ScriptHelper.h"

#include <cassert>
#include <iostream>
#include "addon/scriptbuilder/scriptbuilder.h"
#include "addon/scripthelper/scripthelper.h"
#include "addon/scriptstdstring/scriptstdstring.h"

using namespace Ahreed::Scripting;

ScriptHelper::ScriptHelper() {
    _engine = asCreateScriptEngine();

    _engine->SetMessageCallback(asFUNCTION(ScriptHelper::MessageCallback), 0,
                                asCALL_CDECL);

    RegisterStdString(_engine);
    _engine->RegisterGlobalFunction("void print(const string &in)",
                                    asFUNCTION(ScriptHelper::PrintString),
                                    asCALL_CDECL);

    _ctx = _engine->CreateContext();
}

ScriptHelper::~ScriptHelper() {
    _ctx->Release();
    _engine->ShutDownAndRelease();
}

bool ScriptHelper::loadFile(const std::string &filename) {
    CScriptBuilder builder;

    if (builder.StartNewModule(_engine, "MainModule") < 0) return false;
    if (builder.AddSectionFromFile(filename.c_str()) < 0) return false;
    return builder.BuildModule() >= 0;
}

bool ScriptHelper::runCode(const std::string &code) {
    int r = ExecuteString(_engine, code.c_str(),
                          _engine->GetModule("MainModule"), _ctx);
    return true;
}

void ScriptHelper::MessageCallback(const asSMessageInfo *msg, void *param) {
    std::string type;
    switch (msg->type) {
        case asMSGTYPE_WARNING:
            type = "WARN ";
            break;
        case asMSGTYPE_INFORMATION:
            type = "INFO ";
            break;
        default:
            type = "ERR ";
    }

    printf("%s (%d, %d): %s -> %s\n", msg->section, msg->row, msg->col,
           type.c_str(), msg->message);
}

void ScriptHelper::PrintString(const std::string &msg) { std::cout << msg; }
