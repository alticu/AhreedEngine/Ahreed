#ifndef __BASEWINDOW_H
#define __BASEWINDOW_H

#include <cstdint>

#ifndef MAX_SCENES
#define MAX_SCENES 8
#endif

namespace Ahreed {

namespace Scene {
class BaseScene;
};

namespace Graphics {

class BaseRenderer;

class BaseWindow {
   public:
    BaseWindow();
    ~BaseWindow(){};

    void setScene(Scene::BaseScene *scene, int index = 0);

    virtual void update() = 0;
    virtual void draw() = 0;

    virtual void run() = 0;

    virtual void setSize(int x, int y) = 0;
    virtual void setTitle(const char *title) = 0;
    virtual void setFlags(unsigned char flags) = 0;

    void keyPressedCallback(unsigned int code, uint8_t state);

   protected:
    Ahreed::Scene::BaseScene *_scenes[MAX_SCENES];

    BaseRenderer *_renderer;
};

};  // namespace Graphics
};  // namespace Ahreed

#endif
