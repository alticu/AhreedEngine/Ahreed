#ifndef __GLFWWINDOW_H
#define __GLFWWINDOW_H

#include "BaseWindow.h"

struct GLFWwindow;

namespace Ahreed {
namespace Graphics {

class GlfwWindow : public BaseWindow {
   public:
    GlfwWindow();
    ~GlfwWindow();

    void update();
    void draw();

    void run();

    void setSize(int x, int y){};
    void setTitle(const char *title){};
    void setFlags(unsigned char flags){};

   private:
    BaseRenderer *_renderer;
    GLFWwindow *_win;
};

};  // namespace Graphics
};  // namespace Ahreed

#endif
