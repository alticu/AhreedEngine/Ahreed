#include "BaseWindow.h"

#include <cassert>
#include "../../Scene/BaseScene.h"

using namespace Ahreed::Graphics;

BaseWindow::BaseWindow() {
    for (int i = 0; i < MAX_SCENES; i++) _scenes[i] = nullptr;
}

void BaseWindow::setScene(Ahreed::Scene::BaseScene *scene, int index) {
    assert(index < MAX_SCENES);
    _scenes[index] = scene;
}

void BaseWindow::draw() {
    for (int i = 0; i < MAX_SCENES; i++) {
        if (_scenes[i] != nullptr && _scenes[i]->active)
            _scenes[i]->render(_renderer);
    }
}

void BaseWindow::keyPressedCallback(unsigned int code, uint8_t state) {
    for (int i = MAX_SCENES - 1; i >= 0; i--) {
        if (_scenes[i] != nullptr) {
            if (_scenes[i]->onKeyPressed(code, state)) return;
        }
    }
}
