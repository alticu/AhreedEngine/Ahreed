#include "GlfwWindow.h"

#include <GL/gl3w.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <stdexcept>
#include "../../Scene/BaseScene.h"
#include "../../Service/ServiceManager.h"
#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"

using namespace Ahreed::Graphics;

GlfwWindow::GlfwWindow() : BaseWindow() {
    if (!glfwInit()) throw std::runtime_error("Failed to initialize GLFW!");

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    _win = glfwCreateWindow(640, 480, "Ahreed", NULL, NULL);
    if (!_win) throw std::runtime_error("Failed to create GLFW window!");

    glfwMakeContextCurrent(_win);
    if (gl3wInit()) throw std::runtime_error("Failed to load gl3w!");

    glfwSwapInterval(1);

    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGui_ImplGlfw_InitForOpenGL(_win, true);
    ImGui_ImplOpenGL3_Init();
    ImGui::StyleColorsDark();

    glfwSetKeyCallback(
        _win, [](GLFWwindow* win, int key, int, int action, int mods) {
            Service::CallbackManager::keyPressedCallback(key, action);
            ImGui_ImplGlfw_KeyCallback(win, key, 0, action, mods);
        });
}

GlfwWindow::~GlfwWindow() {
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

    glfwDestroyWindow(_win);
    glfwTerminate();
}

void GlfwWindow::update() {
    glfwPollEvents();
    draw();
}

void GlfwWindow::draw() {
    glfwMakeContextCurrent(_win);

    int width, height;
    glfwGetFramebufferSize(_win, &width, &height);

    glViewport(0, 0, width, height);
    glClearColor(0.45f, 0.55f, 0.60f, 1.00f);
    glClear(GL_COLOR_BUFFER_BIT);

    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();

    BaseWindow::draw();

    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

    glfwMakeContextCurrent(_win);
    glfwSwapBuffers(_win);
}

void GlfwWindow::run() {
    while (!glfwWindowShouldClose(_win)) {
        update();
    }
}
